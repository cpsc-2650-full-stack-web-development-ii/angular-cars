# CPSC2650 - Angular Car Project

A small project developed to demonstrate a containerized application with:  
(1) FeathersJs framework Docker's image working as a backend  
(2) FeathersJs framework Docker's image in the frontend running AngularJs framework  
(3) Nginx Docker's image as a proxy server HTTP requests

## Project Design

![alt text](img/angular_cars.png "Title Text")

## Install

### 0. Prerequisites

- Docker
- Git

This project uses `Docker` to run each part of the application.
To install `Docker` follow the instructions in the
[Docker website](https://www.docker.com/get-started).

Git is a version control system (VCS). To install `git` follow
the instructions in the [Git website](https://git-scm.com/downloads)

### 1. Clone

The first step is to clone the project:

```
git clone https://gitlab.com/cpsc-2650-full-stack-web-development-ii/angular-cars.git
```

Enter the project directory:

```
cd angular-cars/
```

### 2. Checking write permitions

Make sure you have write permissions in both `backend` and `frontend` directories

#### 3. Backend

In the project directory (`angular-cars/`), start the `backend` container:

```
./scripts/start-backend-container.sh
```

Inside the container, `start` the backend compilation script:

```
npm start
```

If everything worked as expected, you should see the message: `info: Feathers application started on http://localhost:3030`.

#### 4. Frontend

In the frontend directory (`angular-cars/frontend`), `start` the frontend compilation script:

```
npm start
```

If everything worked as expected, you should see the message: ` Angular Live Development Server is listening on 0.0.0.0:4200, open your browser on http://localhost:4200/ : Compiled successfully.`

Go back to the project directory (`angular-cars/`), start the `frontend` container:

```
./scripts/start-frontend-container.sh
```

Inside the container, `start` the backend compilation script one more time:

```
npm start
```

If everything worked as expected, you should see the message: ` Angular Live Development Server is listening on 0.0.0.0:4200, open your browser on http://localhost:4200/ : Compiled successfully.`

This extra frontend step prevents common compilation errors that might occur when the `npm start` script is executed inside container

### 5. NGINX

In the project directory (`angular-cars/`), start the `ngnix` container:

```
./scripts/start-nginx-container.sh
```

## Check backend and frontend running

The **backend** is running at `http://localhost:80`
The **frontend** is running at `http://localhost:8080`

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

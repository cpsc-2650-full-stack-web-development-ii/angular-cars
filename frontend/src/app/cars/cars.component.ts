import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Paginated } from '@feathersjs/feathers';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarsComponent implements OnInit {
  cars$: Observable<any[]>;
  carForm;

  constructor(
    private data: DataService,
    private formBuilder: FormBuilder
    
    ) {
    // get things from data service
    this.cars$ = data.cars$().pipe(
        // our data is paginated, so map to .data
        map((t: Paginated<any>) => t.data)
      );
      
    }
    
  deleteCar(carId){
    console.log('car id to be deleted-->', carId)
    this.data.deleteCar(carId);
  }
  
    
  onSubmit(carData) {
    this.data.addCar(carData);
  }

  ngOnInit(): void {
    this.carForm = this.formBuilder.group({

      make: ['', [Validators.required] ],
      model: ['', [Validators.required] ],
      year: ['', [Validators.required] ],
      mileage: ['', [Validators.required] ]

    });
    
  }

}
